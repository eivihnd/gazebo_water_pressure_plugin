#include <gazebo_depth_sensor_plugin/gazebo_depth_sensor.h>
#include <gazebo/common/Events.hh>
#include <gazebo/physics/physics.hh>
#include <boost/bind.hpp>

namespace gazebo {


GazeboRosDepthSensor::GazeboRosDepthSensor()
{
}

// Destructor
GazeboRosDepthSensor::~GazeboRosDepthSensor()
{
	node_handle_->shutdown();
	delete node_handle_;
}

// Load the controller
void GazeboRosDepthSensor::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
{
	world = _model->GetWorld();

	if (_sdf->HasElement("robotNamespace"))
    		namespace_ = _sdf->GetElement("robotNamespace")->GetValue()->GetAsString();
  	else
    		namespace_.clear();

  	if (!_sdf->HasElement("topicName"))
    		topic_ = “waterpressure”;
  	else
    		topic_ = _sdf->GetElement("topicName")->Get<std::string>();


  	if (_sdf->HasElement("bodyName"))
  	{
    		link_name_ = _sdf->GetElement("bodyName")->GetValue()->GetAsString();
    		link = _model->GetLink(link_name_);
  	}
  	else
  	{
    		link = _model->GetLink();
    		link_name_ = link->GetName();
  	}

  	if (!link)
  	{
    		ROS_FATAL("GazeboRosDepthSensor plugin error: bodyName: %s does not exist\n", link_name_.c_str());
    	return;
  	}

	if (_sdf->HasElement("waterDensity"))
	{
		_sdf->GetElement("waterDensity")->GetValue()->Get(this->water_density);
	}
	else
	{
		this->water_density = 1000.0;
	}
	
	
	
	// Start ROS Node
	if (!ros::isInitialized())
	{
		int argc = 0;
		char** argv = NULL;
		ros::init(argc, argv, "gazebo", ros::init_options::NoSigintHandler | ros::init_options::AnonymousName);
	}
	
	node_handle_ = new ros::NodeHandle(namespace_);
	publisher_ = node_handle_->advertise<std_msgs::Float32>(topic_,1);
	
	// Listen to the update event. This event is broadcast every
      	// simulation iteration.
	this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          boost::bind(&GazeboRosDepthSensor::Update, this, _1));
}

void GazeboRosDepthSensor::Reset()
{
}

void GazeboRosDepthSensor::Update(const common::UpdateInfo & /*_info*/)
{
	
	math::Pose pose = link->GetWorldPose();
	
	// get gravity (use only z-direction)
	gazebo::math::Vector3 gravity = world->GetPhysicsEngine()->GetGravity();
	
	// calculate water pressure in Pa
	water_pressure = water_density * gravity.z * pose.pos.z;
	
	std_msgs::Float32 msg;
	msg.data = water_pressure;

	publisher_.publish(msg);
	
}

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(GazeboRosDepthSensor)

} // namespace gazebo
