# gazebo_water_pressure_plugin
A Gazebo plugin that outputs the hydrostatic pressure based on water density (rho) and robot position/depth (h).

## Calculations

```
#!c++

P = pressure [Pa]
rho = fluid density [kg/m^3]
g = gravitational acceleration [m/s^2]
h = height of water column, parallel to the direction of gravity [m]

P = rho * g * h

```

## ROS Topic
This plugin is publishing to the topic name given in the URDF file