#ifndef HECTOR_GAZEBO_PLUGINS_GAZEBO_ROS_DEPTH_SENSOR_H
#define HECTOR_GAZEBO_PLUGINS_GAZEBO_ROS_DEPTH_SENSOR_H

#include <gazebo/common/Plugin.hh>
#include <std_msgs/Float32.h>
#include <gazebo/common/common.hh>
#include <ros/ros.h>
#include <sdf/sdf.hh>


namespace gazebo
{

class GazeboRosDepthSensor : public ModelPlugin
{

public:
  GazeboRosDepthSensor();
  virtual ~GazeboRosDepthSensor();

protected:
  virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);
  virtual void Reset();
  virtual void Update(const common::UpdateInfo & /*_info*/);

private:
  /// \brief The parent World
  physics::WorldPtr world;

  /// \brief The link referred to by this plugin
  physics::LinkPtr link;

  ros::NodeHandle* node_handle_;
  ros::Publisher publisher_;

  float water_pressure;
  float water_density;

  std::string namespace_;
  std::string topic_;
  std::string link_name_;
  std::string frame_id_;

  // Pointer to the update event connection
  event::ConnectionPtr updateConnection;

};

} // namespace gazebo

#endif
